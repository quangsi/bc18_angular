import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-structural-directives',
  template: `
    <div class="container">
      <h2>Ng If dùng để ẩm hiện 1 nội dung hoặc 1 thẻ component</h2>
      <div *ngIf="isLogin; else loginTemplate">
        <p>Wellcome back</p>
        <button class="btn btn-success" (click)="isLogin = !isLogin">
          Logout
        </button>
      </div>
      <!-- <div *ngIf="!isLogin">
        <p>Please log in</p>
        <button class="btn btn-success" (click)="isLogin = !isLogin">
          Login
        </button>
      </div> -->
      <h2>
        Ng template dùng để bọc và đại diện cho 1 thẻ hoặc nhiều thẻ. Sẽ không
        trực tiếp render ra ngoài layout
      </h2>
      <ng-template #loginTemplate>
        <p>Please log in</p>
        <button class="btn btn-success" (click)="isLogin = !isLogin">
          Login
        </button>
      </ng-template>

      <h2>
        Ng switch dùng để render 1 nội dung trong nhiều nội dung dựa vào 1 điều
        kiện
      </h2>

      <div [ngSwitch]="color">
        <p *ngSwitchCase="'red'" class="text-danger">Red color</p>
        <p *ngSwitchCase="'blue'" class="text-primary">Blue color</p>
        <p *ngSwitchDefault>Defaul color</p>
      </div>

      <h2>
        Ng for dùng để duyệt mảng và tạo ra các thẻ hoặc component tương ứng
      </h2>
      <ng-container *ngIf="colors">
        <div *ngFor="let item of colors; index as idx">
          <p>{{ idx + 1 }} - {{ item }}</p>
        </div>
      </ng-container>
    </div>

    <h2>Ng container : dùng để bọc 1 nội dung và ko sinh ra thẻ phụ</h2>
    <!-- <> </> -->
    <ng-container *ngIf="3 < 5">
      <h4>Đã login</h4>
    </ng-container>
  `,
})
export class StructuralDirectivesComponent implements OnInit {
  isLogin: boolean = true;

  color: string = 'blue';

  colors: string[] = ['red', 'green', 'blue'];
  constructor() {}

  ngOnInit() {}
}
