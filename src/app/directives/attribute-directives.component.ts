import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-attribute-directives',
  template: `
    <h1>Attribute directive</h1>

    <h2>Ng class giúp kiểm soát các class được sử dụng vào 1 thẻ</h2>
    <!-- type 1 -->
    <button [ngClass]="stringClass">Login</button>
    <br />
    <!-- type 2 -->
    <button [ngClass]="role == 'admin' ? 'adminClass' : 'userClass'">
      Xoá
    </button>

    <!-- type:3 -->

    <button [class.disable]="role === 'user'">Xoá</button>
    <!-- type 4 multiple -->

    <br />
    <button [ngClass]="{ textRed: role == 'user', bgwhite: age == 18 }">
      Demo multiple class
    </button>
    <h2>Ng style kiểm soát các thuộc tính css trên 1 thẻ</h2>

    <p [ngStyle]="{ color: colorVariable, background: backgroundVariable }">
      Ng style
    </p>
  `,
  styles: [
    `
      .mauchu {
        color: red;
      }
      .fontchu {
        font-size: 40px;
      }

      .adminClass {
        background: green;
        color: white;
        padding: 20px 40px;
      }
      .userClass {
        background: gray;
        text: black;
        padding: 20px 40px;
      }

      .disable {
        background: gray;
        text: black;
        padding: 20px 40px;
      }
      .textRed {
        color: red;
      }
      .bgwhite {
        background: white;
      }
    `,
  ],
})
export class AppAttributeDirectivesComponent implements OnInit {
  stringClass = 'mauchu fontchu';
  role: string = 'user';

  age: number = 18;
  colorVariable: string = 'pink';
  backgroundVariable: string = 'green';
  constructor() {}

  ngOnInit() {}
}
