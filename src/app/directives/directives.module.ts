import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { AppAttributeDirectivesComponent } from './attribute-directives.component';
import { StructuralDirectivesComponent } from './structural-directives.component';
import { AppHighlineDirectiveDirective } from './app-highline-directive.directive';
import { NgContentDemoComponent } from './ng-content-demo/ng-content-demo.component';

@NgModule({
  imports: [CommonModule],
  exports: [
    StructuralDirectivesComponent,
    AppAttributeDirectivesComponent,
    AppHighlineDirectiveDirective,
    NgContentDemoComponent,
  ],
  declarations: [
    StructuralDirectivesComponent,
    AppAttributeDirectivesComponent,
    AppHighlineDirectiveDirective,
    NgContentDemoComponent,
  ],
  providers: [],
})
export class DirectivesModule {}
