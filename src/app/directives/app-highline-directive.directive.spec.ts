/* tslint:disable:no-unused-variable */

import { TestBed, async } from '@angular/core/testing';
import { AppHighlineDirectiveDirective } from './app-highline-directive.directive';

describe('Directive: AppHighlineDirective', () => {
  it('should create an instance', () => {
    const directive = new AppHighlineDirectiveDirective();
    expect(directive).toBeTruthy();
  });
});
