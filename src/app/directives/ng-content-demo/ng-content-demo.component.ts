import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ng-content-demo',
  // ng-content tương tự this.props.childrent bên react
  template: ` <ng-content select="[slot=2]"></ng-content> `,
})
export class NgContentDemoComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
