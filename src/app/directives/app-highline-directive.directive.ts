import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
  OnInit,
} from '@angular/core';

@Directive({
  selector: '[appAppHighlineDirective]',
})
export class AppHighlineDirectiveDirective implements OnInit {
  constructor(private element: ElementRef) {
    console.log(element);
    // element.nativeElement.style.color = 'red';
  }
  ngOnInit(): void {
    this.element.nativeElement.style.color = 'red';
  }
  // binding (gắn) 1 thuộc tính của thẻ vào 1 biến
  @HostBinding('style.color') textColor: string = 'red';

  @HostBinding('value') contentValue: string = '';
  //  gắn sự kiện vào element sự dụng directive
  @HostListener('mouseover')
  changeBgRed() {
    this.element.nativeElement.style.background = 'red';
    // console.log(this.element.nativeElement.style);
  }
  @HostListener('mouseleave') changeBgWhite() {
    this.element.nativeElement.style.background = 'white';
  }
  @HostListener('click') doSomeThing() {
    console.log('yes');
    this.textColor = 'white';

    this.contentValue = 'hello';
  }
}
