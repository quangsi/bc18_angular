import {
  Directive,
  ElementRef,
  HostBinding,
  HostListener,
} from '@angular/core';

@Directive({
  selector: '[app-Hightlinght]',
})
export class HightlinghtDirective {
  constructor(private e: ElementRef) {
    e.nativeElement.style.color = 'red';
  }

  maSacc: string = '';
  @HostBinding('style.color') maSac: string = '';
  @HostListener('mouseenter') doSomething() {
    this.e.nativeElement.style.background = 'red';
    this.maSac = 'green';
  }
  @HostListener('mouseleave') doSomethingg() {
    this.e.nativeElement.style.background = 'white';
  }
}
