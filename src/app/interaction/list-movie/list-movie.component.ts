import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list-movie',
  template: `
    <div class="container">
      <h2>Bài tập movie list</h2>

      <div class="row mt-5">
        <div class="col-4 " *ngFor="let movie of moviesList">
          <app-item-movie
            (onRemoveItem)="handleRemove(movie.id)"
            [data]="movie"
          ></app-item-movie>
        </div>
      </div>
    </div>
  `,
})
export class ListMovieComponent implements OnInit {
  moviesList = [
    {
      tenMon: 'Haddock',
      giaMon: '400.00',
      moTa: 'Molestiae ratione ipsam.',
      id: '14',
    },
    {
      tenMon: 'Pacific anchoveta',
      giaMon: '772.00',
      moTa: 'Adipisci minima officiis.',
      id: '15',
    },
    {
      tenMon: 'Nile perch',
      giaMon: '289.00',
      moTa: 'Eos sequi voluptatem inventore dolores.',
      id: '16',
    },
    {
      tenMon: 'Pacific herring',
      giaMon: '548.00',
      moTa: 'Perferendis quidem accusamus ipsa.',
      id: '17',
    },
    {
      tenMon: 'European anchovy',
      giaMon: '545.00',
      moTa: 'Hic est aliquid vitae quis natus.',
      id: '18',
    },
    {
      tenMon: 'Wuchang bream',
      giaMon: '474.00',
      moTa: 'Et a perferendis qui eveniet non magnam exercitationem enim qui.',
      id: '19',
    },
    {
      tenMon: 'Pacific cod',
      giaMon: '619.00',
      moTa: 'Soluta accusantium consequuntur voluptas id eius eius omnis.',
      id: '20',
    },
    {
      tenMon: 'Rohu',
      giaMon: '84.00',
      moTa: 'Magnam reiciendis ducimus aut illo.',
      id: '21',
    },
    {
      tenMon: 'Yellowfin tuna',
      giaMon: '141.00',
      moTa: 'Omnis velit eum non accusantium et.',
      id: '22',
    },
    {
      tenMon: 'Atlantic menhaden',
      giaMon: '134.00',
      moTa: 'Suscipit ut quo.',
      id: '23',
    },
  ];
  handleRemove(id: string) {
    let index = this.moviesList.findIndex((item) => {
      return item.id == id;
    });

    this.moviesList.splice(index, 1);
  }
  constructor() {}

  ngOnInit() {}
}
