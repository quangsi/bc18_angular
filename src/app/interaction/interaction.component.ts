import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-interaction',

  template: `
    <ng-content></ng-content>
    <!--  truyền trực tiếp giá trị thì ko cần [ ] -->

    <!--  dùng property binding [ ] để truyền 1 variable -->

    <app-chid
      (onChangeTitleEvent)="handleChangeTitle($event)"
      [titleContent]="title"
      variant="bg-primary"
    ></app-chid>
    <br />
    <app-list-movie></app-list-movie>
  `,
})
export class InteractionComponent implements OnInit {
  title: string = 'Cửa hàng bán cá';
  constructor() {}

  handleChangeTitle(value: string) {
    this.title = value;
  }

  ngOnInit() {}
}
// input : truyền từ cha sang con
// output : truyền từ con sang cha
