import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-chid',
  template: `
    <h1 class="{{ variant }}">App child</h1>
    <h4>Variant {{ variant }}</h4>

    <h2>{{ titleContent }}</h2>

    <button (click)="handleChangeTitle()" class="btn btn-success">
      Change title
    </button>
  `,
})
export class ChidComponent implements OnInit {
  @Input() titleContent: string = '';
  @Input() variant: string = '';
  //  định nghĩa tên sự kiện ( click, mouseover)
  @Output() onChangeTitleEvent = new EventEmitter();
  handleChangeTitle() {
    this.onChangeTitleEvent.emit('Cửa hàng bán mèo');
  }
  constructor() {}

  ngOnInit() {
    console.log(this.variant);
  }
}
