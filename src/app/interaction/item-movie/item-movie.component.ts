import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-item-movie',
  template: `
    <div class="card text-left">
      <!-- <img class="card-img-top" [src]="data.hinhAnh" alt="" /> -->
      <div class="card-body">
        <h4 class="card-title">{{ data.id }}</h4>
        <h4 class="card-title">{{ data.tenMon }}</h4>
        <p class="card-text">{{ data.giaMon }}</p>

        <button (click)="handleRemoveItem(data.id)" class="btn btn-danger">
          Xoá movie
        </button>
      </div>
    </div>
  `,
})
export class ItemMovieComponent implements OnInit {
  @Output() onRemoveItem = new EventEmitter();
  @Input() data: any = {};

  handleRemoveItem(id: string) {
    this.onRemoveItem.emit(id);
  }
  constructor() {}

  ngOnInit() {}
}
