import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { InteractionComponent } from './interaction.component';
import { ChidComponent } from './chid/chid.component';
import { ListMovieComponent } from './list-movie/list-movie.component';
import { ItemMovieComponent } from './item-movie/item-movie.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    InteractionComponent,
    ChidComponent,
    ListMovieComponent,
    ItemMovieComponent,
  ],
  exports: [InteractionComponent],
})
export class InteractionModule {}
