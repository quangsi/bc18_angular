import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-movies-list',
  template: `
    <div class="container">
      <h2 class="text-center">Movies List</h2>

      <div class="row">
        <div *ngFor="let item of movies" class="col-3 my-2">
          <div class="card text-left">
            <img class="card-img-top" src="holder.js/100px180/" alt="" />
            <div class="card-body">
              <h4 class="card-title">{{ item.title }}</h4>
              <p class="card-text">{{ item.desc }}</p>
              <button class="btn btn-danger" routerLink="/movies/{{ item.id }}">
                View detail
              </button>
            </div>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./movies-list.component.scss'],
})
export class MoviesListComponent implements OnInit {
  movies: any[] = [
    {
      title: 'Australian Terrier',
      desc: 'Tempora qui inventore facilis quas fugiat.',
      id: '1',
    },
    {
      title: 'English Foxhound',
      desc: 'Accusamus quam blanditiis distinctio ab necessitatibus velit et.',
      id: '2',
    },
    {
      title: 'Field Spaniel',
      desc: 'A repudiandae aut sit enim minima sequi soluta ut.',
      id: '3',
    },
    {
      title: 'Australian Shepherd',
      desc: 'Enim soluta qui voluptatem beatae earum deserunt illum ullam.',
      id: '4',
    },
    {
      title: 'Eurasier',
      desc: 'Aliquid quia libero eveniet eius suscipit.',
      id: '5',
    },
    {
      title: 'Dogue de Bordeaux',
      desc: 'Iste ut eos iusto rerum.',
      id: '6',
    },
    {
      title: 'Galician Shepherd Dog',
      desc: 'Odit exercitationem et vero sed a voluptas eaque animi.',
      id: '7',
    },
    {
      title: 'Basset Hound',
      desc: 'Exercitationem ut dolore.',
      id: '8',
    },
    {
      title: 'Braque Francais',
      desc: 'Quo ducimus dolorum fugiat qui suscipit provident est in.',
      id: '9',
    },
    {
      title: 'Neapolitan Mastiff',
      desc: 'Neque dignissimos consequatur qui consectetur optio.',
      id: '10',
    },
    {
      title: 'Pshdar Dog',
      desc: 'Ab mollitia et autem.',
      id: '11',
    },
    {
      title: 'Japanese Spitz',
      desc: 'Necessitatibus et ipsum veritatis ea ea architecto.',
      id: '12',
    },
    {
      title: 'Podenco Canario',
      desc: 'Consequatur delectus unde voluptatibus tempore dolorum.',
      id: '13',
    },
    {
      title: 'Karelian Bear Dog',
      desc: 'Maiores itaque eius aspernatur praesentium mollitia sapiente.',
      id: '14',
    },
    {
      title: 'Sporting Lucas Terrier',
      desc: 'Aut qui corrupti exercitationem labore repellendus.',
      id: '15',
    },
  ];
  constructor() {}

  ngOnInit() {}
}
