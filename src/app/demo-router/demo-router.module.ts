import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DemoRouterComponent } from './demo-router.component';
import { AboutPageComponent } from './about-page/about-page.component';
import { HomePageComponent } from './home-page/home-page.component';
import { NotFoundPageComponent } from './not-found-page/not-found-page.component';
import { MoviesListComponent } from './movies-list/movies-list.component';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { RouterModule, Routes } from '@angular/router';
import { RouterLayoutComponent } from './router-layout/router-layout.component';

const routesChildren: Routes = [
  {
    path: '',
    component: RouterLayoutComponent,
    children: [
      {
        path: 'about',
        component: AboutPageComponent,
      },
      {
        path: 'movies',
        component: MoviesListComponent,
      },
      {
        path: 'movies/:id',
        component: MovieItemComponent,
      },
      {
        path: '404',
        component: NotFoundPageComponent,
      },
      {
        path: '**',
        redirectTo: '404',
      },
    ],
  },
];
@NgModule({
  imports: [CommonModule, RouterModule.forChild(routesChildren)],
  declarations: [
    DemoRouterComponent,
    AboutPageComponent,
    HomePageComponent,
    NotFoundPageComponent,
    MoviesListComponent,
    MovieItemComponent,
    RouterLayoutComponent,
  ],
  exports: [
    AboutPageComponent,
    HomePageComponent,
    NotFoundPageComponent,
    MoviesListComponent,
    MovieItemComponent,
    RouterLayoutComponent,
  ],
})
export class DemoRouterModule {}
