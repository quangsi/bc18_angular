import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-movie-item',
  template: ` <h1 class="text-center">Item {{ idMovie }}</h1>`,
  styleUrls: ['./movie-item.component.scss'],
})
export class MovieItemComponent implements OnInit {
  constructor(private activatedRotue: ActivatedRoute) {}

  idMovie: number = 0;
  // life cycle Angular
  ngOnInit() {
    let { id } = this.activatedRotue.snapshot.params;

    this.idMovie = id;

    console.log(this.idMovie);

    console.log('this.activatedRotue', this.activatedRotue);
  }
}
