import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bai-tap-render-list',
  template: ` <div class="container py-5">
    <h2 appAppHighlineDirective>Bài tập renderlist</h2>
    <div class="my-5">
      <div class="form-group">
        <label for="">Mã</label>
        <input
          appAppHighlineDirective
          type="text"
          name=""
          id=""
          class="form-control"
          placeholder=""
          aria-describedby="helpId"
          #idSp
        />
      </div>
      <div class="form-group">
        <label for="">Tên </label>
        <input
          [value]="'hello'"
          type="text"
          name=""
          id=""
          class="form-control"
          placeholder=""
          aria-describedby="helpId"
          #nameSp
        />
      </div>
      <div class="form-group">
        <label for="">Giá</label>
        <input
          type="text"
          name=""
          id=""
          class="form-control"
          placeholder=""
          aria-describedby="helpId"
          #priceSp
        />
      </div>
      <div class="form-group">
        <label for="">Mô tả</label>
        <input
          type="text"
          name=""
          id=""
          class="form-control"
          placeholder=""
          aria-describedby="helpId"
          #descriptionSP
        />
      </div>
      <button
        (click)="
          handleAddSp(
            idSp.value,
            nameSp.value,
            priceSp.value,
            descriptionSP.value
          )
        "
        class="btn btn-success"
      >
        Thêm sản phẩm
      </button>
    </div>
    <table class="table">
      <thead>
        <td>Mã</td>
        <th>Tên mo1n</th>
        <th>Giá món</th>
        <th>Mô tả</th>
      </thead>
      <tbody>
        <tr
          *ngFor="let movie of moviesList; index as i"
          [class.rowGray]="i % 2 == 0"
        >
          <td>{{ movie.id }}</td>
          <td>{{ movie.tenMon }}</td>
          <td>{{ movie.giaMon }}</td>
          <td>{{ movie.moTa }}</td>
        </tr>
      </tbody>
    </table>
  </div>`,
  styles: [
    `
      .rowGray {
        background: rgb(218, 218, 218);
      }
    `,
  ],
})
export class BaiTapRenderListComponent implements OnInit {
  moviesList = [
    {
      tenMon: 'Haddock',
      giaMon: '400.00',
      moTa: 'Molestiae ratione ipsam.',
      id: '14',
    },
    {
      tenMon: 'Pacific anchoveta',
      giaMon: '772.00',
      moTa: 'Adipisci minima officiis.',
      id: '15',
    },
    {
      tenMon: 'Nile perch',
      giaMon: '289.00',
      moTa: 'Eos sequi voluptatem inventore dolores.',
      id: '16',
    },
    {
      tenMon: 'Pacific herring',
      giaMon: '548.00',
      moTa: 'Perferendis quidem accusamus ipsa.',
      id: '17',
    },
    {
      tenMon: 'European anchovy',
      giaMon: '545.00',
      moTa: 'Hic est aliquid vitae quis natus.',
      id: '18',
    },
    {
      tenMon: 'Wuchang bream',
      giaMon: '474.00',
      moTa: 'Et a perferendis qui eveniet non magnam exercitationem enim qui.',
      id: '19',
    },
    {
      tenMon: 'Pacific cod',
      giaMon: '619.00',
      moTa: 'Soluta accusantium consequuntur voluptas id eius eius omnis.',
      id: '20',
    },
    {
      tenMon: 'Rohu',
      giaMon: '84.00',
      moTa: 'Magnam reiciendis ducimus aut illo.',
      id: '21',
    },
    {
      tenMon: 'Yellowfin tuna',
      giaMon: '141.00',
      moTa: 'Omnis velit eum non accusantium et.',
      id: '22',
    },
    {
      tenMon: 'Atlantic menhaden',
      giaMon: '134.00',
      moTa: 'Suscipit ut quo.',
      id: '23',
    },
  ];

  handleAddSp(id: string, ten: string, gia: string, moTa: string) {
    console.log({
      id,
      ten,
      gia,
      moTa,
    });
    this.moviesList.push({
      id: id,
      tenMon: ten,
      giaMon: gia,
      moTa: moTa,
    });
  }
  constructor() {}

  ngOnInit() {}
}
