import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaiTapLayoutSecondComponent } from './bai-tap-layout-second.component';

describe('BaiTapLayoutSecondComponent', () => {
  let component: BaiTapLayoutSecondComponent;
  let fixture: ComponentFixture<BaiTapLayoutSecondComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaiTapLayoutSecondComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaiTapLayoutSecondComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
