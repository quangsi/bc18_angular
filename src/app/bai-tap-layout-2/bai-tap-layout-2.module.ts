import { NgModule } from '@angular/core';
import { HeaderComponent } from './header/header.component';
import { SliderComponent } from './slider/slider.component';
import { FooterComponent } from './footer/footer.component';
import { ItemComponent } from './item/item.component';
import { BaiTapLayoutSecondComponent } from './bai-tap-layout-second/bai-tap-layout-second.component';

@NgModule({
    imports: [],
    exports: [BaiTapLayoutSecondComponent],
    declarations: [
    HeaderComponent,
    SliderComponent,
    FooterComponent,
    ItemComponent,
    BaiTapLayoutSecondComponent
  ],
    providers: [],
})
export class BaiTapLayout2Module { }
