import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-data-binding',
  template: `
    <h1>Data Binding Hello CyberSoft</h1>

    <h2>Interpolation</h2>
    <p>Message : {{ message }}</p>
    <p>Student name - age: {{ name }} - {{ age }}</p>
    <img src="{{ imgUrl }}" alt="" />

    <h2>Property binding</h2>
    <img [src]="imgUrl" alt="" />
    <input type="text" [value]="username" />

    <h2>Event binding</h2>

    <button class="btn btn-success" (click)="sayHello()">Click me</button>
    <p>Input value : {{ inputValue }}</p>
    <!-- <input type="text" (input)="hangleGetName($event)" /> -->
    <input type="text" (change)="hangleGetName($event)" />

    <h3>Template Variable</h3>
    <p>Name: {{ name }}</p>
    <input type="text" #inputRef />

    <button (click)="handleChangeName(inputRef.value)">Change name</button>

    <br />

    <p>Description: {{ description }}</p>

    <input type="text" [(ngModel)]="description" />
  `,
})
export class DataBindingComponent implements OnInit {
  message: string = 'Hello Việt Nam';
  name: string = 'Alice';

  age: number = 18;

  imgUrl =
    'https://www.highlandscoffee.com.vn/vnt_upload/product/03_2018/menu-PSD_3.png';

  username: string = 'Bob';

  inputValue: string = '';

  description: string = '';
  sayHello() {
    console.log('Hello');
  }

  hangleGetName(event: Event) {
    //    as : ép kiểu
    const { value } = event.target as HTMLInputElement;

    console.log(value);

    this.inputValue = value;
    // đồng bộ
  }

  handleChangeName(name: string) {
    console.log(name);
    this.name = name;
  }

  constructor() {}

  ngOnInit() {}
}

// danh sách danh bạ
interface IContact {
  name: string;
  phone: number;
}

let contract1: IContact = {
  name: 'Alice',
  phone: 234,
};

let listContact: IContact[] = [contract1];
