import { Component, OnInit, ViewChild } from '@angular/core';
import { DemoViewChildComponent } from './demo-view-child/demo-view-child.component';

@Component({
  selector: 'app-view-child-view-children',
  template: `
    <h1>Demo view child</h1>
    <app-demo-view-child #viewChildRef></app-demo-view-child>

    <button (click)="handleShowViewChild()" class="btn btn-success">
      ShowViewChild
    </button>

    <button (click)="handleClickChild()">Button Parent</button>

    <app-demo-view-children></app-demo-view-children>
  `,
  styleUrls: ['./view-child-view-children.component.scss'],
})
export class ViewChildViewChildrenComponent implements OnInit {
  // gọi tới component con từ component cha, tương tự khi dom 1 thẻ html.
  @ViewChild('viewChildRef') viewChilComp!: DemoViewChildComponent;

  handleShowViewChild() {
    this.viewChilComp.usernameViewchild = 'Bob';
    console.log(this.viewChilComp.usernameViewchild);
  }

  handleClickChild() {
    this.viewChilComp.handleRemoveName();
  }

  constructor() {}

  ngOnInit() {}
}
