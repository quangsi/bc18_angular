import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-seat-item',
  template: ` <button class="btn btn-primary">{{ seat.id }}</button> `,
  styleUrls: ['./seat-item.component.scss'],
})
export class SeatItemComponent implements OnInit {
  @Input() seat: any = {};

  seatName = ' Alice';
  constructor() {}

  ngOnInit() {}
}
