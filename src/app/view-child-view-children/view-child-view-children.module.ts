import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewChildViewChildrenComponent } from './view-child-view-children.component';
import { DemoViewChildComponent } from './demo-view-child/demo-view-child.component';
import { DemoViewChildrenComponent } from './demo-view-children/demo-view-children.component';
import { SeatItemComponent } from './seat-item/seat-item.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    ViewChildViewChildrenComponent,
    DemoViewChildComponent,
    DemoViewChildrenComponent,
    SeatItemComponent,
  ],
  exports: [ViewChildViewChildrenComponent],
})
export class ViewChildViewChildrenModule {}
