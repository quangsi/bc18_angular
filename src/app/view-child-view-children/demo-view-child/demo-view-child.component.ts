import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-demo-view-child',
  template: `{{ usernameViewchild }}

    <button class="btn btn-danger" (click)="handleRemoveName()">
      Button Child
    </button> `,
  styleUrls: ['./demo-view-child.component.scss'],
})
export class DemoViewChildComponent implements OnInit {
  usernameViewchild: string = 'Aclice';
  constructor() {}
  handleRemoveName() {
    this.usernameViewchild = '';
  }
  ngOnInit() {}
}
