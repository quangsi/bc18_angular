import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AntdModuleComponent } from './antd-module.component';
import { NzFormModule } from 'ng-zorro-antd/form';

@NgModule({
  imports: [CommonModule, NzFormModule],
  declarations: [AntdModuleComponent],
  exports: [NzFormModule],
})
export class AntdModuleModule {}
