import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-bai-tap-form',
  template: `<div class="container">
    <h1>Bài tập form</h1>

    <p>Email</p>
    <input type="text" [(ngModel)]="email" />
    <p>Full name</p>
    <input type="text" #inputFullname />
    <br />
    <button
      class="btn btn-success mt-4"
      (click)="handleGetFullname(inputFullname.value)"
    >
      Submit
    </button>

    <p>Email: {{ email }}</p>
    <p>Fullname: {{ fullname }}</p>
  </div> `,
})
export class BaiTapFormComponent implements OnInit {
  email: string = '';
  fullname: string = '';

  handleGetFullname(value: string) {
    this.fullname = value;
  }
  constructor() {}

  ngOnInit() {}
}
