import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-login-page',
  template: `
    <div class="container mx-auto py-5">
      <form nz-form class="login-form">
        <nz-form-item>
          <nz-form-control nzErrorTip="Please input your taiKhoan!">
            <nz-input-group nzPrefixIcon="user">
              <input
                type="text"
                nz-input
                formControlName="taiKhoan"
                placeholder="taiKhoan"
              />
            </nz-input-group>
          </nz-form-control>
        </nz-form-item>
        <nz-form-item>
          <nz-form-control nzErrorTip="Please input your matKhau!">
            <nz-input-group nzPrefixIcon="lock">
              <input
                type="matKhau"
                nz-input
                formControlName="matKhau"
                placeholder="matKhau"
              />
            </nz-input-group>
          </nz-form-control>
        </nz-form-item>
        <div nz-row class="login-form-margin">
          <div nz-col [nzSpan]="12">
            <label nz-checkbox formControlName="remember">
              <span>Remember me</span>
            </label>
          </div>
          <div nz-col [nzSpan]="12">
            <a class="login-form-forgot">Forgot matKhau</a>
          </div>
        </div>
        <button
          nz-button
          class="login-form-button login-form-margin"
          [nzType]="'primary'"
        >
          Log in
        </button>
        Or
        <a>register now!</a>
      </form>
    </div>
  `,
  styles: [
    `
      .login-form {
        max-width: 300px;
      }

      .login-form-margin {
        margin-bottom: 16px;
      }

      .login-form-forgot {
        float: right;
      }

      .login-form-button {
        width: 100%;
      }
    `,
  ],
})
export class LoginPageComponent implements OnInit {
  constructor() {}

  ngOnInit(): void {}
}
