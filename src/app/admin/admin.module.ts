import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminComponent } from './admin.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { AuthService } from './auth.service';
import { AuthGuard } from '../auth.guard';
import { RouterModule, Routes } from '@angular/router';
import { AntdModule } from '../antd/antd.module';
import { MoviePageComponent } from './movie-page/movie-page.component';
import { ReactiveFormsModule } from '@angular/forms';
let route: Routes = [
  {
    path: 'login',
    component: LoginPageComponent,
  },
  {
    path: '',
    component: MoviePageComponent,
    canActivate: [AuthGuard],
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(route),
    AntdModule,
    ReactiveFormsModule,
  ],
  declarations: [AdminComponent, MoviePageComponent, LoginPageComponent],
  providers: [AuthService, AuthGuard],
})
export class AdminModule {}
