import { NgModule } from '@angular/core';
import { BaiTapLayoutComponent } from './bai-tap-layout/bai-tap-layout.component';
import { ContentComponent } from './content/content.component';
import { FooterComponent } from './footer/footer.component';
import { HeaderComponent } from './header/header.component';
import { SidebarComponent } from './sidebar/sidebar.component';


@NgModule({
    imports: [],
    exports: [BaiTapLayoutComponent],
    declarations: [HeaderComponent,FooterComponent,SidebarComponent,ContentComponent,BaiTapLayoutComponent],
    providers: [],
})
export class BaiTapLayou1Module { }
