import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MovieListComponent } from './movie-list/movie-list.component';
import { MovieDetailComponent } from './movie-detail/movie-detail.component';
import { MovieDataDumService } from './movie-data-dum.service';
import { MovieItemComponent } from './movie-item/movie-item.component';
import { MovieServiceService } from './movie-service.service';
import { HttpClientModule } from '@angular/common/http';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzCardModule } from 'ng-zorro-antd/card';
import { AntdModule } from '../antd/antd.module';
let root: Routes = [
  {
    path: '',
    children: [
      {
        path: '',
        component: MovieListComponent,
      },
      {
        path: ':id',
        component: MovieDetailComponent,
      },
    ],
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(root),
    HttpClientModule,
    NzButtonModule,
    NzCardModule,
    AntdModule,
  ],
  declarations: [MovieListComponent, MovieItemComponent, MovieDetailComponent],
  providers: [MovieDataDumService, MovieServiceService],
})
export class MovieModule {}
