/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { MovieDataDumService } from './movie-data-dum.service';

describe('Service: MovieDataDum', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [MovieDataDumService]
    });
  });

  it('should ...', inject([MovieDataDumService], (service: MovieDataDumService) => {
    expect(service).toBeTruthy();
  }));
});
