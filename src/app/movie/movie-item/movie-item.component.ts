import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'app-movie-item',
  template: `
    <!-- <div style="height:300px" class="card text-left">
      <img class="card-img-top" src="holder.js/100px180/" alt="" />
      <div class="card-body">
        <h4 class="card-title">{{ movie.biDanh }}</h4>
        <p class="card-text">{{ movie.hinhAnh }}</p>
        <p>Sắp chiếu {{ movie.sapChieu }}</p>
        <button nz-button nzType="primary">Primary Button</button>
      </div>
    </div> -->

    <nz-card nzHoverable style="width:100%" [nzCover]="coverTemplate">
      <nz-card-meta [nzTitle]="movie.biDanh" nzDescription="www.instagram.com">
      </nz-card-meta>
    </nz-card>
    <ng-template #coverTemplate>
      <img
        style="height:200px ; object-fit:cover"
        alt="example"
        [src]="movie.hinhAnh"
      />
      <button nz-button nzType="primary">Xem chi tiết</button>
    </ng-template>
  `,
  styleUrls: ['./movie-item.component.scss'],
})
export class MovieItemComponent implements OnInit, OnChanges {
  @Input() movie: any = {};
  constructor() {}

  ngOnInit() {}

  ngOnChanges() {
    console.log('mvoie', this.movie);
  }
}
