import { Component, OnInit } from '@angular/core';
import { MovieDataDumService } from '../movie-data-dum.service';
import { MovieServiceService } from '../movie-service.service';

@Component({
  selector: 'app-movie-list',
  template: `
    <div class="container">
      <input nz-input placeholder="Basic usage" />
      <div class="row">
        <div class="col-3 " *ngFor="let movie of movieList">
          <div class="py-3">
            <app-movie-item [movie]="movie"></app-movie-item>
          </div>
        </div>
      </div>
    </div>
  `,
  styleUrls: ['./movie-list.component.scss'],
})
export class MovieListComponent implements OnInit {
  constructor(
    movieDataDumService: MovieDataDumService,
    private movieService: MovieServiceService
  ) {
    // this.movieList = movieDataDumService.getMovieList();
    // console.log(this.movieService.getMovieList());
    this.movieService.getMovieList().subscribe(
      (result) => {
        console.log('result', result);
        this.movieList = result;
      },
      (err) => {
        console.log('lỗi !', err);
      }
    );
    // this.movieService.getMovieList()

    console.log('this.movieList', this.movieList);
  }

  movieList: any[] = [];
  ngOnInit() {}
}
