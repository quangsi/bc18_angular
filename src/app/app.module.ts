import { NgModule } from '@angular/core';
import { BaiTapRenderListComponent } from './bai-tap-render-list/bai-tap-render-list.component';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { WellcomeComponent } from './wellcome/wellcome.component';
import { HelloComponent } from './hello/hello.component';
import { BaiTapLayou1Module } from './bai-tap-layout-1/bai-tap-layout-1.module';
import { BaiTapLayout2Module } from './bai-tap-layout-2/bai-tap-layout-2.module';
import { DataBindingModule } from './data-binding/data-binding.module';
import { BaiTapFormComponent } from './bai-tap-4/bai-tap-form.component';
import { FormsModule } from '@angular/forms';
import { DirectivesModule } from './directives/directives.module';
import { InteractionModule } from './interaction/interaction.module';
import { ViewChildViewChildrenModule } from './view-child-view-children/view-child-view-children.module';
import { BaiTapBusModule } from './bai-tap-bus/bai-tap-bus.module';
import { DemoRouterModule } from './demo-router/demo-router.module';
import { Routes, RouterModule } from '@angular/router';
// import { HomePageComponent } from './demo-router/home-page/home-page.component';
// import { AboutPageComponent } from './demo-router/about-page/about-page.component';
// import { NotFoundPageComponent } from './demo-router/not-found-page/not-found-page.component';
// import { MoviesListComponent } from './demo-router/movies-list/movies-list.component';
// import { MovieItemComponent } from './demo-router/movie-item/movie-item.component';
import { RouterLayoutComponent } from './demo-router/router-layout/router-layout.component';
import { AuthenticationModule } from './authentication/authentication.module';
import { LoginPageComponent } from './authentication/login-page/login-page.component';
import { HomePageComponent } from './demo-router/home-page/home-page.component';
import { NZ_I18N } from 'ng-zorro-antd/i18n';
import { en_GB } from 'ng-zorro-antd/i18n';
import { registerLocaleData } from '@angular/common';
import en from '@angular/common/locales/en';
import { HttpClientModule } from '@angular/common/http';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminModule } from './admin/admin.module';

registerLocaleData(en);

//  đi định nghĩa các url cho từng component
const routesApp: Routes = [
  // {
  //   path: '',
  //   loadChildren: () => {
  //     return import('./demo-router/dem
  //       return m.DemoRouterModule;
  //     });
  //   },
  // },
  {
    path: '',
    component: HomePageComponent,
  },
  {
    path: 'demo',
    loadChildren: () => {
      return import('./demo-router/demo-router.module').then(
        (m) => m.DemoRouterModule
      );
    },
  },
  {
    path: 'authentication',
    loadChildren: () => {
      return import('./authentication/authentication.module').then(
        (m) => m.AuthenticationModule
      );
    },
  },
  {
    path: 'movie',
    loadChildren: () => {
      return import('./movie/movie.module').then((m) => m.MovieModule);
    },
  },
  {
    path: 'admin',
    loadChildren: () => {
      return import('./admin/admin.module').then((m) => m.AdminModule);
    },
  },
];

// HighOrderComponent HOC

// nơi quản lý các component, và không có xử lý logic tại đây

// Component khi tạo ra phải đi khai báo, và bắt buộc phải duy nhất 1 component quản lý

// imports: nơi import các lib (thư viện) mà các component được quản lý bởi module cần sử dụng
@NgModule({
  declarations: [
    AppComponent,
    WellcomeComponent,
    HelloComponent,
    BaiTapFormComponent,
    BaiTapRenderListComponent,
  ],
  imports: [
    BrowserModule,
    BaiTapLayou1Module,
    BaiTapLayout2Module,
    DataBindingModule,
    FormsModule,
    DirectivesModule,
    InteractionModule,
    ViewChildViewChildrenModule,
    BaiTapBusModule,
    RouterModule.forRoot(routesApp),
    HttpClientModule,
    BrowserAnimationsModule,
    // DemoRouterModule,
    // AuthenticationModule,
  ],
  //  dùng để khai báo service
  providers: [{ provide: NZ_I18N, useValue: en_GB }],
  bootstrap: [AppComponent],
})
export class AppModule {}
