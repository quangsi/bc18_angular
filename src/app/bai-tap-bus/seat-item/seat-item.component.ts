import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-seat-item',
  template: `<button
    [class.btn-danger]="seatItemProps.booked"
    [class.btn-success]="isSelected"
    [disabled]="seatItemProps.booked"
    class="btn btn-secondary btn-bus"
    (click)="handleIsSelected()"
  >
    {{ seatItemProps.name }}
  </button>`,
  styles: [
    `
      .btn-bus {
        width: 70px;
        heigh: 70px;
        margin: 10px;
      }
    `,
  ],
})
export class SeatItemComponent implements OnInit {
  @Input() seatItemProps: any = {};

  @Output() onSelect = new EventEmitter();
  isSelected: boolean = false;
  constructor() {}

  handleIsSelected() {
    this.isSelected = !this.isSelected;
    this.onSelect.emit({
      seat: this.seatItemProps,
      isSelected: this.isSelected,
    });
  }

  ngOnInit() {}
}
