import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaiTapBusComponent } from './bai-tap-bus.component';
import { SeatListComponent } from './seat-list/seat-list.component';
import { SeatItemComponent } from './seat-item/seat-item.component';
import { SeatSelectedComponent } from './seat-selected/seat-selected.component';

@NgModule({
  imports: [CommonModule],
  declarations: [
    BaiTapBusComponent,
    SeatListComponent,
    SeatItemComponent,
    SeatSelectedComponent,
  ],
  exports: [BaiTapBusComponent],
})
export class BaiTapBusModule {}
