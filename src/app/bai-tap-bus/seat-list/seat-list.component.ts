import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  QueryList,
  SimpleChanges,
  ViewChildren,
} from '@angular/core';
import { SeatItemComponent } from '../seat-item/seat-item.component';

@Component({
  selector: 'app-seat-list',
  template: `
    <div class="row">
      <div class="col-3" *ngFor="let seat of seatsProps">
        <app-seat-item
          #seatItemRef
          (onSelect)="handleSelectedSeate($event)"
          [seatItemProps]="seat"
        ></app-seat-item>
      </div>
    </div>
  `,
  styleUrls: ['./seat-list.component.scss'],
})
export class SeatListComponent implements OnInit, OnChanges {
  @Input() seatsProps: any = {};
  @Output() onSelectSeat = new EventEmitter();

  @ViewChildren('seatItemRef') seatItemListComp!: QueryList<SeatItemComponent>;
  usernameSeatList = 'Alice';
  handleSelectedSeate(dataSeat: any) {
    console.log('dataSeat', dataSeat);

    this.onSelectSeat.emit(dataSeat);
  }
  constructor() {}

  ngOnInit() {}
  ngOnChanges() {
    console.log('seats props', this.seatsProps);
  }

  handleRemoveItem(seatId: number) {
    console.log('id seat item', seatId);
    console.log('seatItemListComp', this.seatItemListComp);

    this.seatItemListComp.forEach((item) => {
      console.log(item.isSelected);

      if (item.seatItemProps.id == seatId) {
        item.isSelected = false;
      }
    });
  }
}
