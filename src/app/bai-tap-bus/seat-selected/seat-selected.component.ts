import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-seat-selected',
  template: `
    <div>
      <h2>Danh sách ghế</h2>

      <h4 *ngIf="seatSelectedListProps.length" class="text-warning">
        Ghế đã đặt: {{ seatSelectedListProps.length }}
      </h4>
      <p *ngFor="let seat of seatSelectedListProps">
        Ghế : <span class="text-danger">{{ seat.name }}</span> - Giá :
        <span class="text-danger ">{{ seat.price }}</span>
        <button (click)="handleRemove(seat.id)" class="btn btn-danger mx-2">
          [Huỷ]
        </button>
      </p>
    </div>
  `,
  styleUrls: ['./seat-selected.component.scss'],
})
export class SeatSelectedComponent implements OnInit {
  @Input() seatSelectedListProps: any[] = [];

  @Output() onRemove = new EventEmitter();

  handleRemove(id: number) {
    this.onRemove.emit(id);
  }
  constructor() {}

  ngOnInit() {}
}
