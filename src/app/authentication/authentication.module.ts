import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AuthenticationComponent } from './authentication.component';
import { LoginPageComponent } from './login-page/login-page.component';
import { RegisterPageComponent } from './register-page/register-page.component';
import { AuthenticationLayoutComponent } from './authentication-layout/authentication-layout.component';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UpdateProfileComponent } from './update-profile/update-profile.component';
import { SurveyPageComponent } from './survey-page/survey-page.component';
import { TitleLenghtPipe } from './titleLenght.pipe';

const routes: Routes = [
  {
    path: '',
    component: AuthenticationLayoutComponent,
    children: [
      {
        path: 'login',
        component: LoginPageComponent,
      },
      {
        path: 'register',
        component: RegisterPageComponent,
      },
      {
        path: 'update-profile',
        component: UpdateProfileComponent,
      },
      {
        path: 'survey',
        component: SurveyPageComponent,
      },
    ],
  },
];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    FormsModule,
    ReactiveFormsModule,
  ],
  declarations: [
    TitleLenghtPipe,

    AuthenticationComponent,
    LoginPageComponent,
    RegisterPageComponent,
    AuthenticationLayoutComponent,
    UpdateProfileComponent,
    SurveyPageComponent,
    TitleLenghtPipe,
  ],
})
export class AuthenticationModule {}
