import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-register-page',
  template: `
    <h2>Register page</h2>

    <div class="container">
      <form action="" #regiserForm="ngForm" (ngSubmit)="handleRegister()">
        <div class="form-group">
          <label for="">Username</label>
          <input
            type="text"
            class="form-control"
            name="username"
            ngModel
            #usernameRef="ngModel"
            maxlength="10"
            minlength="5"
            required
          />
        </div>

        <div
          class="alert alert-danger"
          *ngIf="usernameRef.errors?.['minlength'] || usernameRef.errors?.['maxlength']   || usernameRef.errors?.['required'] && usernameRef.touched"
        >
          Username không hợp lệ
        </div>
        <div class="form-group">
          <label for="">Password</label>
          <input
            type="text"
            class="form-control"
            name="passwrod"
            ngModel
            #passwordRef="ngModel"
            maxlength="20"
            minlength="5"
            required
            pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*d)(?=.*[@$!%*?&])[A-Za-zd@$!%*?&]{8,}$"
          />
        </div>
        <div
          class="alert alert-danger"
          *ngIf="passwordRef.errors?.['minlength'] || passwordRef.errors?.['maxlength']   || passwordRef.errors?.['required'] && passwordRef.touched || passwordRef.errors?.['pattern']"
        >
          Password không hợp lệ
        </div>
        <div class="form-group">
          <label for="">Note</label>
          <input
            type="text"
            class="form-control"
            name="note"
            ngModel
            #noteRef="ngModel"
            minlength="20"
            maxlength="100"
            required
          />
        </div>

        <div
          class="alert alert-danger"
          *ngIf="noteRef.errors?.['minlength'] || noteRef.errors?.['maxlength']   || noteRef.errors?.['required'] && noteRef.touched "
        >
          Note không hợp lệ
        </div>
        <button class="btn btn-warning">Register</button>
      </form>
    </div>
  `,
  styleUrls: ['./register-page.component.scss'],
})
// đăng kí

// username độ dài từ 5 - 10
// password   gồm ít nhất 8 kí tự,  1 chữ số,  1 chữ hoa , 1 chữ thường
// note ít nhất 20 kí tự
export class RegisterPageComponent implements OnInit {
  handleRegister() {
    console.log('register', this.regiserForm.value);

    if (this.regiserForm.valid) {
      console.log('hợp lệ');
    }
    if (this.regiserForm.invalid) {
      console.log('khôn hợp lệ');
    }
  }
  @ViewChild('regiserForm') regiserForm!: NgForm;
  constructor() {}

  ngOnInit() {}
}

// driven form

// reactive form
