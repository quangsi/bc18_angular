import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-authentication-layout',
  template: `
    <div class="container">
      <h2 class="text-warning text-center">Authenticaiton</h2>

      <div>
        <button class="btn btn-warning mx-2" routerLink="/authentication/login">
          Login
        </button>
        <button
          class="btn btn-danger mx-2"
          routerLink="/authentication/register"
        >
          Regiser
        </button>
        <button
          class="btn btn-primary mx-2"
          routerLink="/authentication/update-profile"
        >
          Update Profile
        </button>
        <button
          class="btn btn-secondary mx-2"
          routerLink="/authentication/survey"
        >
          Survey
        </button>
      </div>
    </div>
    <router-outlet></router-outlet>
  `,
  styleUrls: ['./authentication-layout.component.scss'],
})
export class AuthenticationLayoutComponent implements OnInit {
  constructor() {}

  ngOnInit() {}
}
