import { Component, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';
import { MovieService } from '../movie.service';
import { QuanLySpServiceService } from '../quanLySpService.service';

@Component({
  selector: 'app-register-reactive-page',
  templateUrl: './register-reactive-page.component.html',
  styleUrls: ['./register-reactive-page.component.scss'],
})
export class RegisterReactivePageComponent implements OnInit {
  registerForm!: FormGroup;
  movieTitle: any = '';
  dataList: any[] = [];
  constructor(
    private fb: FormBuilder,
    moviesService: MovieService,
    movie: QuanLySpServiceService
  ) {
    this.movieTitle = moviesService.getMoviesTitle();
    movie.getMovies().subscribe((data) => {
      console.log('servuce', data);
    });
  }

  ngOnInit() {
    this.initializeFrom();
  }
  initializeFrom(): void {
    this.registerForm = this.fb.group(
      {
        name: ['', [Validators.required, this.validateURL]],
        phoneNumber: '',
        refrences: this.fb.array([this.fb.control('')]),

        mk: this.fb.group({
          password: ['', [Validators.required]],
          confirmpassword: [
            '',
            [Validators.required, this.comparePassword],
            // Validators.pattern(/^(.{0,7}|[^0-9]*|[^A-Z]*|[^a-z]*|[a-zA-Z0-9]*)$/),
          ],
        }),
      },
      { validatior: this.soSanh() }
    );
  }
  onSubmit(): void {
    console.log(this.registerForm.get('name'));
  }
  comparePassword(control: AbstractControl, form: FormGroup) {
    console.log(form.control.get('name'));
    let password = this.registerForm.get('password');
    console.log('password', password);

    return true;
  }
  validateURL(control: AbstractControl) {
    console.log(
      "control.value.startsWith('https')",
      control.value.startsWith('https')
    );

    if (!control.value.startsWith('https')) {
      return { invalidURL: true };
    }
    return null;
  }
}
