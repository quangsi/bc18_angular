import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-update-profile',
  template: `
    <h2 class="text-primary text-center">Update profile</h2>
    <div class="container">
      <form #formProfile="ngForm" (ngSubmit)="handleUpDate()">
        <div class="form-group">
          <label for="">Username</label>
          <input
            type="text"
            class="form-control"
            name="username"
            id=""
            placeholder=""
            [(ngModel)]="userProfile.username"
            required
            maxlength="10"
            minlength="3"
          />
          <div
            class="alert alert-danger"
            *ngIf="formProfile.control.get('username')?.errors?.['required'] || formProfile.control.get('username')?.errors?.['minlength']"
          >
            Username không hợp lệ
          </div>
        </div>
        <div class="form-group">
          <label for="">Password</label>
          <input
            type="text"
            class="form-control"
            name="password"
            id=""
            placeholder=""
            [(ngModel)]="userProfile.password"
          />
        </div>
        <div class="form-group">
          <label for="">Phone</label>
          <input
            type="text"
            class="form-control"
            name="phone"
            id=""
            placeholder=""
            [(ngModel)]="userProfile.phone"
          />
        </div>
        <div class="form-group">
          <label for="">email</label>
          <input
            type="text"
            class="form-control"
            name="email"
            id=""
            placeholder=""
            [(ngModel)]="userProfile.email"
            pattern="[a-z0-9._%+-]+@[a-z0-9.-]+.[a-z]{2,4}$"
            #emailRef="ngModel"
          />
        </div>
        <div *ngIf="emailRef?.errors?.['pattern']" class="alert alert-danger">
          Email không hợp lệ
        </div>
        <div class="form-group">
          <label for="">Note</label>
          <input
            type="text"
            class="form-control"
            name="note"
            id=""
            placeholder=""
            required
            maxlength="10"
            minlength="5"
            [(ngModel)]="userProfile.note"
            #noteRef="ngModel"
          />
        </div>

        <!-- <div
          *ngIf="formProfile.control.get('note')?.errors?.['required'] && formProfile.control.get('note')?.['touched']"
          class="alert alert-danger"
        >
          Note không hợp lệ
        </div> -->

        <!-- <div
          *ngIf="noteRef?.errors?.['required'] && noteRef?.['touched']"
          class="alert alert-danger"
        >
          Note không hợp lệ
        </div> -->
        <div
          *ngIf="noteRef?.errors?.['required'] && noteRef?.['touched']"
          class="alert alert-danger"
        >
          Note không được để rỗng
        </div>

        <div
          *ngIf="noteRef?.errors?.['minlength'] || noteRef?.errors?.['maxlength'] "
          class="alert alert-danger"
        >
          Note phải chứa từ 5 tới 10 kí tự
        </div>
        <button class="btn btn-primary">Update</button>
      </form>
      <pre>
        {{ userProfile | json }}
      </pre
      >
    </div>
  `,

  styles: [
    `
      input.ng-invalid.ng-touched {
        color: red;
        border-left: 3px solid red;
      }
      input.ng-valid.ng-touched {
        color: blue;
        border-left: 3px solid green;
      }
    `,
  ],
})
export class UpdateProfileComponent implements OnInit {
  @ViewChild('formProfile') formProfile!: NgForm;

  emailParten =
    /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  handleUpDate() {
    // console.log('form', this.formProfile.value);
    // console.log('userProfile', this.userProfile);
    if (this.formProfile.invalid) {
      console.log('không hợp lệ');

      return;
    }
    console.log(this.formProfile.control.get('email'));
  }
  userProfile = {
    username: 'Alice',
    phone: 38678,
    password: '123456',
    email: 'alice@gmail.com',
    note: '',
  };
  constructor() {}

  ngOnInit() {}
}
