import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'titleLenght',
})
export class TitleLenghtPipe implements PipeTransform {
  transform(value: any, limit: number = 30): any {
    console.log('value pile', value);

    if (value.length > limit) {
      return value.slice(0, limit) + '...';
    } else {
      return value;
    }
  }
}
