import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-login-page',
  template: `
    <div class="container">
      <h1>Login page</h1>
      <form #formLogin="ngForm" (ngSubmit)="handleLoginForm()">
        // sau khi import formModule, thẻ form thành directive quản lý các thẻ
        input
        <div class="form-group">
          <label for="">Username </label>
          <input
            type="text"
            name="username"
            id=""
            class="form-control"
            placeholder=""
            aria-describedby="helpId"
            ngModel
            required
          />
        </div>
        <div class="form-group">
          <label for="">Password </label>
          <input
            type="text"
            name="password"
            id=""
            class="form-control"
            placeholder=""
            aria-describedby="helpId"
            ngModel
            required
          />
        </div>
        <button class="btn btn-danger">Login</button>
      </form>
    </div>
  `,
  styleUrls: ['./login-page.component.scss'],
})
export class LoginPageComponent implements OnInit {
  @ViewChild('formLogin') formLogin!: NgForm;
  constructor() {}

  handleLoginForm() {
    // console.log('YEs', this.formLogin.value);
    // console.log('form', this.formLogin.control);
    console.log('form', this.formLogin);

    // gọi tới 1 thẻ input
    // người dùng đã foucus vào ô input
    console.log('1 input', this.formLogin.control.get('username'));

    console.log(this.formLogin.control.get('username')?.touched);
  }

  ngOnInit() {}
}
