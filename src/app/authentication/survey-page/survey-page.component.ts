import { Component, OnChanges, OnInit } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-survey-page',
  template: `
    <div class="container">
      <h2 class="text-center">Reactive form</h2>

      <pre>{{ surveyForm.value | json }}</pre>

      <h4>Date: {{ currentDay | date: 'dd/MM/YYYY' }}</h4>

      <div>Title : {{ title | titleLenght: 200 }}</div>
      <form action="" [formGroup]="surveyForm" (ngSubmit)="onSubmit()">
        <div class="form-group">
          <label for="">{{ 'Username' | uppercase }}</label>
          <input
            type="text"
            class="form-control"
            name=""
            id=""
            aria-describedby="helpId"
            placeholder=""
            formControlName="username"
          />

          <div
            *ngIf="surveyForm.get('username')?.errors?.['required'] &&surveyForm.get('username')?.touched"
          >
            Username không hợp lệ
          </div>
        </div>
        <div class="form-group">
          <label for="">FB link</label>
          <input
            type="text"
            class="form-control"
            name=""
            id=""
            aria-describedby="helpId"
            placeholder=""
            formControlName="urlFB"
          />

          <div
            *ngIf="surveyForm.get('urlFB')?.errors?.['urlFbInvalid'] && surveyForm.get('urlFB')?.touched"
            class=" text-danger"
          >
            URL không hợp lệ
          </div>
        </div>
        <div class="form-group">
          <label for="">Email</label>
          <input
            type="text"
            class="form-control"
            name=""
            id=""
            aria-describedby="helpId"
            placeholder=""
            formControlName="email"
          />
        </div>

        <div formGroupName="family">
          <div class="form-group">
            <label for="">Child Name </label>
            <input
              type="text"
              class="form-control"
              name=""
              id=""
              formControlName="childName"
              aria-describedby="helpId"
              placeholder=""
            />
          </div>

          <div *ngIf="surveyForm.get('family.childName')?.errors?.['required']">
            Child Name không được để rỗng
          </div>
          <div class="form-group">
            <label for="">Gender </label>
            <input
              type="text"
              class="form-control"
              name=""
              id=""
              formControlName="gender"
              aria-describedby="helpId"
              placeholder=""
            />
          </div>
        </div>
        <button class="btn btn-warning">Submit</button>
      </form>
    </div>
  `,
  styleUrls: ['./survey-page.component.scss'],
})
export class SurveyPageComponent implements OnInit, OnChanges {
  surveyForm!: FormGroup;
  constructor(private fb: FormBuilder) {
    this.surveyForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      email: ['', [Validators.pattern(/\S+@\S+\.\S+/)]],
      urlFB: ['', [this.validateURL]],
      family: this.fb.group({
        childName: ['Bob', [Validators.required]],
        gender: 'Femail',
      }),
    });
  }

  currentDay = Date.now();
  title: string = `Angular is an application design framework and development platform for creating efficient and sophisticated single-page apps.

These Angular docs help you learn and use the Angular framework and development platform, from your first application to optimizing complex single-page apps for enterprises. Tutorials and guides include downloadable examples to accelerate your projects.`;
  onSubmit() {
    console.log(this.surveyForm.get('urlFB'));
  }
  validateURL(control: AbstractControl) {
    console.log('control', control.value);

    if (!control.value.startsWith('https')) {
      return {
        urlFbInvalid: true,
      };
    }
    return null;
  }
  ngOnInit() {}

  ngOnChanges() {
    console.log('yes', this.currentDay);
  }
}

{
  username: 'Aclice';
  urlFB: 'https://aliceFB.com';

  family: {
    child: 'Bob';
    gender: 'Male';
  }
}
{
  username: 'Aclice';
  urlFB: 'https://aliceFB.com';
  family: {
    child: 'Bob';
    gender: 'Male';
  }
}
